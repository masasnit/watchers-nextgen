import dateutil.parser
import logging
import optparse
import time
import requests
import sys
import json
from geojson import Point, LineString, Polygon, MultiPolygon

from datetime import datetime
import xml.etree.ElementTree as ET

from EMPSUtils.MASAS.Entry.Entry import Entry
from watchers.core.WatcherService import WatcherService
from watchers.core.EntryData import EntryData


class ServiceBCHighway( WatcherService ):

    def __init__( self, debug = False, options = None  ):
        super( ServiceBCHighway, self ).__init__( debug = debug, options = options )

        app_dir = self._configData["app_dir"]

    # end __init__()

    def _getExternalData( self ):
        lastQueryDT = self._serviceDataMgr.serviceData.lastQueryDateTime
        lastQueryDTFormatted = lastQueryDT.strftime( "%Y-%m-%dT%H:%M:%SZ")

        url = str( self._configData["feed_url"] )
        currentQueryDT = datetime.utcnow()

        feed = None
        feedText = None
        try:
            feed = requests.get( url )
        except Exception, ex:
            self.logger.error( "Data request failed: " + ex.message )
        # end try

        if( feed is not None ):
            feedText = feed.text
            self._serviceDataMgr.serviceData.lastQueryDateTime = currentQueryDT
        # end if

        return feedText
    # end _getExternalData()

    def _processExternalData( self, externalData ):
        entries = []
        #Load JSON objects
        root = json.loads(externalData)
        for r in root:
            entries.append(r)

        return entries
    # end _processExternalData()

    def _processExternalDataItem( self, dataItem ):
        masasItem = None

        # Don't process the data by default...
        processData = True

        # Get the timestamp from the data item...
        updatedDT = dateutil.parser.parse( dataItem[8], ignoretz=True )

        entryInfo = self._serviceDataMgr.findEntryBySourceId(  str(dataItem[13]) )
        if( entryInfo != None and updatedDT <= entryInfo['updated'] ):
            # Don't update the data unless it's new...
            processData = False
        # end if

        if( processData == True):
            # Create the business object...
            masasItem = EntryData()

            # Have we seen this item before? If so, we need the associated Entry ID...
            if( entryInfo != None ):
                masasItem.masasIdentifier = entryInfo['entryId']
            # end if

            # Create the Entry...
            entry = Entry()

            #Add Categories
            entry.categories.append("Transport")
            entry.status = "Actual"
            entry.icon = "ems/incident/roadway/roadwayClosure"
            #Add content block
            entry.setContent(dataItem[9])
            #TODO: Add appropriate title for event
            entry.setTitle(dataItem[0])
            #TODO: Replace use of geojson for geometry
            entry.geometry = Point((dataItem[1], dataItem[2]))

            # Set the source identifier
            masasItem.sourceIdentifier = dataItem[13]

            # Set the source data for future lookups (saved to DB)
            # TODO: this needs to be added!
            masasItem.sourceData = str(dataItem)
            # Set the data to publish...
            masasItem.data = entry
        # end if

        return masasItem
    # end _processExternalDataItem()

# end ServiceBCHighway

if __name__ == '__main__':

    usage = "usage: service_BCHighway [-d] [-m] [-c]"

    parser = optparse.OptionParser(usage)
    parser.add_option("-d", action="store_true", dest="debug",
                      help="print debugging messages")
    parser.add_option("-m", action="store_true", dest="monitor",
                      help="run as a monitoring daemon")
    parser.add_option("-c", action="store", type="string", dest="configFileLocation",
                      help="Config file location.")
    (options, args) = parser.parse_args()

    service = None
    try:
        service = ServiceBCHighway( debug = options.debug, options = options )
    except Exception, ex:
        service.close()
        logging.error( "An unhandled error occurred during the creation of the Service: " + ex.message )
        sys.exit()
    # end try

    # TODO - MOVE THIS INTO THE BASE WATCHER CLASS...
    if( options.debug ):
        service.run()
    else:
        try:
            service.run()
        except Exception, ex:
            service.close()
            logging.error( "An unhandled error occurred in the Service: " + ex.message )
            sys.exit()
        # end try
    # end if

    # run in monitoring mode if requested
    if options.monitor:
        min_int = int( service.check_interval )
        sec_int = min_int * 60
        service.logger.debug( "Monitor mode: updating every %d minutes" %min_int )
        while 1:
            time.sleep( sec_int )

            if( options.debug ):
                service.run()
            else:
                try:
                    service.run()
                except Exception, ex:
                    service.close()
                    logging.error( "An unhandled error occurred in the Service: " + ex.message )
                    sys.exit()
                # end try
            # end if

        # end while
    # end if

    if( service ):
        service.close()
    # end if

# end if