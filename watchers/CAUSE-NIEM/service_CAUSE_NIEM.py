import logging
import optparse
import time
import requests
import sys
import json
import os
from datetime import datetime, timedelta

from lxml import etree
from geojson import Point

from watchers.core.EntryData import EntryData
from watchers.core.WatcherService import WatcherService
from EMPSUtils.MASAS.Entry.Entry import Entry


class ServiceExample( WatcherService ):

    # ServiceExample Constructor
    def __init__( self, debug = False, options = None  ):
        super( ServiceExample, self ).__init__( debug = debug, options = options )

        app_dir = self._configData["app_dir"]

        # Open/Read the NIMS/MASAS Icon lookup file...
        lookup_file_path = os.path.join( ".", "nimsDefToMASASIcon.json" )
        with open( lookup_file_path ) as lookupFile:
            self._symbolLookups = json.load( lookupFile )
        # end with

    # end __init__()

    # _getExternalData()
    #
    # Overloaded from the base class.
    # Get the data from the external feed and return it in any format to be
    # parsed by _processExternalData()
    #
    # @return Object - All the data to be parsed.
    def _getExternalData( self ):
        # Get the the time of the last query...
        lastQueryDT = self._serviceDataMgr.serviceData.lastQueryDateTime

        # Build the url we will need...
        url = str( self._configData["feed_url"] )

        feed = None
        feedText = None

        try:
            feed = requests.get( url )
        except Exception, ex:
            self.logger.error( "Data request failed: " + ex.message )
        # end try

        if( feed is not None ):
            # We want the raw data of the feed for now...
            feedText = feed.content

            # Update the query time...
            self._serviceDataMgr.serviceData.lastQueryDateTime = datetime.utcnow()
        # end if

        return feedText
    # end _getExternalData()

    # _processExternalData()
    #
    # Overloaded from the base class.
    # From the "externalData" object, extract the individual items.
    # Return the objects as an array to be parsed by the
    # _processExternalDataItem method.
    #
    # @input externalData Object - An Object containing all the items to be extracted.
    # @return Array - The extracted items.
    def _processExternalData( self, externalData ):
        feedItems = []

        root = etree.XML( externalData )
        feedItems = root.findall( "{http://mitre.org/emevent/0.6/cad2cad/}Event" )

        return feedItems
    # end _processExternalData()

    # _processExternalDataItem()
    #
    # Overloaded from the base class.
    # From the "dataItem" object, extract the contents and populate
    # a AlertData or EntryData with the necessary fields.
    #
    # @input dataItem Object - A a single item containing an Entry/Alert.
    # @return BaseDataItem - The Entry/Alert to be published.
    def _processExternalDataItem( self, dataItem ):

        namespaces = { "cad": "http://mitre.org/emevent/0.6/cad2cad/",
                       "nc": "http://release.niem.gov/niem/niem-core/3.0/",
                       "mof": "http://example.com/milops/1.1/",
                       "gml": "http://www.opengis.net/gml/3.2" }

        # Create the business object...
        masasItem = EntryData()

        # Get the Item identifier...
        dataIdEl = dataItem.find("./cad:ResourcePackageDetail/cad:ResourceIdentifier/nc:IdentificationID", namespaces)

        # Set the source identifier
        if( dataIdEl is not None ):
            masasItem.sourceIdentifier = dataIdEl.text
        else:
            masasItem.sourceIdentifier = None
        # end if

        # Have we seen this item before? If so, we need the associated Entry ID...
        if( masasItem.sourceIdentifier is not None ):
            masasItem.masasIdentifier = self._serviceDataMgr.findEntryIdBySourceId( masasItem.sourceIdentifier )
        # end if

        # Create the Entry...
        entry = Entry()

        # Populate the Entry with data...

        # Set the defaults...
        entry.status = self._configData.get( "masas_status", "Exercise" )

        # Set the Title...
        titleEl = dataItem.find( "./cad:ResourcePackageDetail/cad:ResourceName", namespaces )

        if( titleEl is not None ):
            entry.setTitle( titleEl.text )

        # Set the Content ...
        contentEl = dataItem.find( "./cad:ResourcePackageDetail/cad:ResourcePackageDescription", namespaces )

        if( contentEl is not None ):
            entry.setContent( contentEl.text )

        # Set the Location...
        eventLocEl = dataItem.find( "mof:EventLocation", namespaces )
        if( eventLocEl is not None ):
            pointEl = eventLocEl.find( ".//gml:pos", namespaces )
            if( pointEl is not None ):
                points = pointEl.text.split(' ')
                entry.geometry = Point( ( float(points[1]), float(points[0]) ) )
            # end if
        # end if

        # Set the MASAS Icon...
        nimsDefEl = dataItem.find( "./cad:ResourcePackageDetail/cad:ResourcePackageNIMSDefinition", namespaces )
        if( nimsDefEl is not None ):
            entry.icon = self._lookupNIMSDefinition( nimsDefEl.text )
        # end if

        # Set the Expires date...
        # TODO: Should we use Event/EventValidityDateTimeRage/EndDate/DateTime ????
        #       For now uses a admin configured value in minutes.
        expiresInMin = int( self._configData.get( "masas_expires", 30 ) )
        entry.expires = datetime.utcnow() + timedelta( minutes=expiresInMin )

        # Set the source data for future lookups (saved to DB)
        masasItem.sourceData = etree.tostring( dataItem )

        # Set the data to publish...
        masasItem.data = entry

        # Done!
        return masasItem
    # end _processExternalDataItem()

    def _lookupNIMSDefinition( self, value ):
        # Get the: if not available, return a default Value.
        return self._symbolLookups.get( value, "other" )
    # end _lookupNIMSDefinition

# end ServiceExample

if __name__ == '__main__':

    usage = "usage: service_CAUSE_NIEM [-d] [-m] [-c]"

    parser = optparse.OptionParser(usage)
    parser.add_option("-d", action="store_true", dest="debug",
                      help="print debugging messages")
    parser.add_option("-m", action="store_true", dest="monitor",
                      help="run as a monitoring daemon")
    parser.add_option("-c", action="store", type="string", dest="configFileLocation",
                      help="Config file location.")
    (options, args) = parser.parse_args()

    service = None
    try:
        service = ServiceExample( debug = options.debug, options = options )
    except Exception, ex:
        service.close()
        logging.error( "An unhandled error occurred during the creation of the Service: " + ex.message )
        sys.exit()
    # end try

    # TODO - MOVE THIS INTO THE BASE WATCHER CLASS...
    if( options.debug ):
        service.run()
    else:
        try:
            service.run()
        except Exception, ex:
            service.close()
            logging.error( "An unhandled error occurred in the Service: " + ex.message )
            sys.exit()
        # end try
    # end if

    # run in monitoring mode if requested
    if options.monitor:
        min_int = int( service.check_interval )
        sec_int = min_int * 60
        service.logger.debug( "Monitor mode: updating every %d minutes" %min_int )
        while 1:
            time.sleep( sec_int )

            if( options.debug ):
                service.run()
            else:
                try:
                    service.run()
                except Exception, ex:
                    service.close()
                    logging.error( "An unhandled error occurred in the Service: " + ex.message )
                    sys.exit()
                # end try
            # end if

        # end while
    # end if

    if( service ):
        service.close()
    # end if

# end if