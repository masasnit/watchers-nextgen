from datetime import datetime, date, time, timedelta

class ServiceData( object ):

    def __init__( self, serviceDataManager, watcherName ):
        self._serviceDataMgr = serviceDataManager
        self._isNew = True
        self.watcherName = watcherName

        weekDelta = timedelta( weeks = 1 )
        dayDelta = timedelta( days = 1 )
        self.lastQueryDateTime = datetime.utcnow() - weekDelta
    # end __init__()

    def save( self ):
        self._serviceDataMgr.save()
    # end save

    @staticmethod
    def createFromDBRecord( serviceDataManager, watcherName, record ):
        serviceData = ServiceData( serviceDataManager, watcherName )
        if( record is not None ):
            serviceData.lastQueryDateTime = record[1]
            serviceData._isNew = False
        # end if

        return serviceData
    # end createFromDBRecord

# end ServiceData()