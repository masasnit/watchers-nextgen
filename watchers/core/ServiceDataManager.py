from datetime import datetime, timedelta
from string import Template

import psycopg2
import psycopg2.extras
import psycopg2.errorcodes

from watchers.core.ServiceData import ServiceData


class ServiceDataManager( object ):

    def __init__( self, watcherService, watcherName, host = None, port = None, database = None, user = None, password = None ):
        psycopg2.extras.register_uuid()

        self._watcherSvc = watcherService
        self._logger = watcherService.logger
        self.serviceData = None

        # Connect to the database...
        self._dbConn = None

        try:
            self._dbConn = psycopg2.connect( host=host, port=port, database=database, user=user, password=password )
        except Exception, ex:
            self._logger.error( "An error occurred during the creation of the database connection: " + ex.message )
        # end try

        if( self._dbConn is None ):
            # Raise the error!
            raise RuntimeError( "The ServiceDataManger could not connect to the Database.  See the logs for more details!" )

        # Load the base data that we need from the db...
        self._getServiceDataFromDB( watcherName )

    # end __init__()

    def _getServiceDataFromDB( self, watcherName ):
        watcherData = None

        with self._dbConn:
            with self._dbConn.cursor() as cursor:
                cursor.execute( "SELECT * FROM watchers WHERE source_watcher_id=%s LIMIT 1;", ( watcherName, ) )
                watcherData = cursor.fetchone()
            # end with
        # end with

        self.serviceData = ServiceData.createFromDBRecord( self, watcherName, watcherData )
    # end _getServiceDataFromDB

    def findEntryIdBySourceId( self, sourceIdentifier ):
        retValue = None

        with self._dbConn:
            with self._dbConn.cursor() as cursor:
                cursor.execute( "SELECT entry_id FROM watcher_data WHERE source_watcher_item_id=%s LIMIT 1;", ( sourceIdentifier, ) )
                queryData = cursor.fetchone()
            # end with
        # end with

        if( queryData is not None ):
            retValue = queryData[0]

        return retValue
    # end findEntryIdBySourceId()

    def findEntryBySourceId( self, sourceIdentifier ):
        entryInfo = None

        with self._dbConn:
            with self._dbConn.cursor() as cursor:
                cursor.execute( "SELECT entry_id, updated, digest FROM watcher_data WHERE source_watcher_item_id=%s LIMIT 1;", ( sourceIdentifier, ) )
                queryData = cursor.fetchone()
            # end with
        # end with

        if( queryData is not None ):
            entryInfo = {
                'entryId': queryData[0],
                'updated': queryData[1],
                'digest': queryData[2]
            }
        # end if

        return entryInfo
    # end findEntryBySourceId()

    def removeItemsOlderThan(self, nbrOfDays):

        olderThanDT = datetime.today() - timedelta(days=nbrOfDays)

        with self._dbConn:
            with self._dbConn.cursor() as cursor:
                cursor.execute("DELETE FROM watcher_data WHERE updated < %s;", (olderThanDT,))
                self._logger.info( str(cursor.rowcount) + " items have been deleted from the database.")
            # end with
        # end with

    # end removeItemsOlderThan()

    def save( self ):
        if( self.serviceData is not None ):
            # Let's insert/update (UPSERT) the data...
            queryTmpStr = """
                            UPDATE watchers SET last_query_time='$queryTime' WHERE source_watcher_id='$serviceId';
                            INSERT INTO watchers (source_watcher_id, last_query_time)
	                            SELECT '$serviceId', '$queryTime'
	                            WHERE NOT EXISTS (SELECT 1 FROM watchers WHERE source_watcher_id='$serviceId');
	                        """

            queryTemplate = Template( queryTmpStr )
            queryStr = queryTemplate.substitute( queryTime = self.serviceData.lastQueryDateTime, serviceId=self.serviceData.watcherName )

            with self._dbConn:
                with self._dbConn.cursor() as cursor:
                    cursor.execute( queryStr )
                # end with
            # end with

        # end if
    # end save

    def savePublishedItem( self, watcher_data ):
        # This will add and update the data...

        query = """
                UPDATE watcher_data SET source_watcher_id='$srcWatcherId', source_watcher_item_id='$srcWatcherItemId', source_item_data=$srcItemData, updated='$lastUpdated', digest='$digest'
                    WHERE entry_id='$entryId';
                INSERT INTO watcher_data (entry_id, source_watcher_id, source_watcher_item_id, source_item_data, updated, digest)
                    SELECT '$entryId', '$srcWatcherId', '$srcWatcherItemId', $srcItemData, '$lastUpdated', '$digest'
                    WHERE NOT EXISTS (SELECT 1 FROM watcher_data WHERE entry_id='$entryId');
                """
        queryTemplate = Template( query )
        queryStr = queryTemplate.substitute( entryId=watcher_data.masasIdentifier, srcWatcherId=self.serviceData.watcherName,
                                             srcWatcherItemId=watcher_data.sourceIdentifier, srcItemData='$$' + watcher_data.sourceData + '$$',
                                             lastUpdated=watcher_data._lastUpdated, digest=watcher_data.digest )

        with self._dbConn:
            with self._dbConn.cursor() as cursor:
                cursor.execute( queryStr )
            # end with
        # end with

    # end savePublishedItem

    # Close any open connection or object....
    def close( self ):
        if( self._dbConn is not None ):
            self._dbConn.close()
            self._dbConn = None
        # end if
    # end close()

# end ServiceDataManager()