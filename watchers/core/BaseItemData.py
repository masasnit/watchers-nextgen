from datetime import datetime

class BaseItemData(object):

    def __init__( self ):
        self.masasIdentifier = None
        self.sourceIdentifier = []
        self.data = None
        self.sourceData = None
        self.digest = None
        self._lastUpdated = datetime.now()

    # end __init__()

    def sourceIdExists( self, sourceIdentifier ):
        retValue = False

        for curSourceId in self.sourceIdentifier:
            if curSourceId == sourceIdentifier:
                retValue = True
                break
            # end if
        # end for

        return retValue
    # end findBySourceId()

# end Class BaseItemData()
