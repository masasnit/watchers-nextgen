import json
import logging
import logging.config
import os
import sys

import requests

from EMPSUtils.MASAS.MASASClient import MASASClient
from watchers.core.EntryData import EntryData
from watchers.core.AlertData import AlertData
from ServiceDataManager import ServiceDataManager


class WatcherService( object ):

    def __init__( self, debug = False, options = None ):

        # Setup internal support variables...
        self._debug = debug                                 # Debug messages turned on/off.
        self._config_data_file = "config.json"              # Set the name of the config data file.
        self._configData = None                             # Init the config data object.

        # Setup the file paths...
        self._defaultPath = "."
        self._systemSettingPath = "."

        dirName = os.path.dirname( sys.argv[0] )
        topLevelName = os.path.split( dirName )[1]
        self._systemSettingPath = os.path.join( "/etc/masas/watchers", topLevelName )

        # Setup the service logging...
        self._setup_logging( options )

        # Config system file path...
        config_user_file_path = os.path.join( self._systemSettingPath, self._config_data_file )

        # Config default file path...
        config_default_file_path = os.path.join( self._defaultPath, self._config_data_file )

        # Open/Read the config file...

        # If a cmd line config param exists, use that only...
        if( options and options.configFileLocation ):
            if( os.path.isfile( options.configFileLocation ) ):
                with open( options.configFileLocation ) as configFile:
                    self._configData = json.load( configFile )
                # end with
            pass
        else:
            # Otherwise use the defaults..
            # Try the system file path...
            if( os.path.isfile( config_user_file_path ) ):
                with open( config_user_file_path ) as configFile:
                    self._configData = json.load( configFile )
                # end with
            # end if

            if( ( self._configData is None ) and ( os.path.isfile( config_default_file_path ) ) ):
                # Try the default file path...
                with open( config_default_file_path ) as configFile:
                    self._configData = json.load( configFile )
                # end with
            # end if
        #end if

        if self._configData is None:
            raise RuntimeError( "No configuration data." )
        # end if

        # Setup internal support variables based on the configuration file...
        self._watch_name = self._configData["watch_name"]           # User defined Watch name
        if self._watch_name is None:
            self._watch_name = __name__                             # Default to the module name

        self._app_dir = self._configData["app_dir"]                 # Current application directory

        # Setup member variables based on the configuration file...
        self.check_interval = self._configData["check_interval"]    # Run interval value

        # Load/Initialize the previous run's data...
        self._serviceDataMgr = ServiceDataManager( self, self._watch_name,
                                                   host = self._configData["host"], port = self._configData["port"],
                                                   database = self._configData["database"],
                                                   user = self._configData["user"],password = self._configData["password"] )

    # end __init__()

    def close(self):
        self._serviceDataMgr.close()
    # end close

    def _setup_logging( self, options ):
        logConfigFileName = "logging.conf"
        logConfigSystemFilePath = os.path.join( self._systemSettingPath, logConfigFileName )
        logConfigDefaultFilePath = os.path.join( self._defaultPath, logConfigFileName )

        validLogConfigPath = logConfigDefaultFilePath

        # Try the system file path...
        if( os.path.isfile( logConfigSystemFilePath ) ):
            validLogConfigPath = logConfigSystemFilePath
        # end if

        # If a cmd line config param exists, use that only...
        if( options and options.configFileLocation ):
            path, file = os.path.split( options.configFileLocation )
            logConfigUserFilePath = os.path.join( path, logConfigFileName )

            if( os.path.isfile( logConfigUserFilePath ) ):
                validLogConfigPath = logConfigUserFilePath
            # end if
        # end if

        logging.config.fileConfig( validLogConfigPath )
        self.logger = logging.getLogger( 'watcherService' )

    # end _setup_logging(self):

    def run( self ):
        externalDataItems = []
        url = self._configData["masas_hub_url"]
        secret = self._configData["masas_hub_secret"]

        masas_client = MASASClient( url, secret )

        externalData = self._getExternalData()

        if( externalData is not None ):
            externalDataItems = self._processExternalData( externalData )
        # end if

        for item in externalDataItems:
            masas_entry_identifier = None
            dataItem = self._processExternalDataItem( item )

            if dataItem is not None:
                errorOccurred = False
                isNewItem = dataItem.masasIdentifier is None

                # todo - Clean this up to be a retry loop that calls "Publish".  In the Publish method check to see if it's new and an entry/alert...
                if isinstance( dataItem, EntryData ):
                    if isNewItem is True:
                        try:
                            masas_entry_identifier = masas_client.add_entry( dataItem.data )
                            self.logger.debug( "Added an Entry! (MASAS_ENTRY_ID=" + masas_entry_identifier + ")" )
                        except ValueError as ex:
                            self.logger.error( "An occurred during in masas_client.add_entry(): " + ex.message )
                            self.logger.debug( "Data content:" )
                            self.logger.debug( dataItem.data )
                            errorOccurred = True
                        # end try
                    else:
                        try:
                            masas_entry_identifier = masas_client.update_entry( dataItem.masasIdentifier, dataItem.data )
                            self.logger.debug( "Updated an Entry! (MASAS_ENTRY_ID=" + masas_entry_identifier + ")" )
                        except ValueError as ex:
                            self.logger.error( "An occurred during in masas_client.update_entry(): " + ex.message )
                            self.logger.debug( "Data content:" )
                            self.logger.debug( dataItem.data )
                            errorOccurred = True
                        # end try
                    # end if
                elif isinstance( dataItem, AlertData ):
                    if isNewItem is True:
                        try:
                            masas_entry_identifier = masas_client.add_alert_XML( dataItem.data )
                            self.logger.info( "Added an Alert! (MASAS_ENTRY_ID=" + masas_entry_identifier + ")" )
                        except ValueError as ex:
                            self.logger.error( "An occurred during in masas_client.add_alert_XML(): " + ex.message )
                            self.logger.debug( "Data content:" )
                            self.logger.debug( dataItem.data )
                            #if "Duplicate content entry" in ex.message:
                                # The alert has been pushed in already... Have to see what we can do with this!
                            errorOccurred = True
                            # end if
                        # end try
                    else:
                        try:
                            masas_entry_identifier = masas_client.update_alert_XML( dataItem.masasIdentifier, dataItem.data )
                            self.logger.info( "Updated an Alert! (MASAS_ENTRY_ID=" + masas_entry_identifier + ")" )
                        except ValueError as ex:
                            self.logger.error( "An occurred during in masas_client.update_alert_XML(): " + ex.message )
                            self.logger.debug( "Data content:" )
                            self.logger.debug( dataItem.data )
                            errorOccurred = True
                        # end try
                    # end if
                # end if

                dataItem.masasIdentifier = masas_entry_identifier

                # if no error occurred
                if not errorOccurred:
                    # Save the item for the next run...
                    self._serviceDataMgr.savePublishedItem( dataItem )
                #end if

            # end if

        # end for

        # Save the Service Data...
        self._serviceDataMgr.save()
    #end run()

    def _getExternalData( self ):
        feedData = requests.get( self._configData["feed_url"] )
        return feedData.text

    def _processExternalData( self, externalData ):
        return []

    def _processExternalDataItem( self, dataItem ):
        return None

# end WatcherService
