import dateutil.parser
import logging
import optparse
import requests
import sys
import time
import xml.etree.ElementTree as ET
import os
from datetime import datetime

current_dir = os.path.dirname("/usr/share/masas-watchers/naads/")
import site
site.addsitedir(current_dir)

from watchers.core.AlertData import AlertData
from watchers.core.WatcherService import WatcherService
from feedItem import FeedItem

class ServiceNAADS( WatcherService ):

    # ServiceNAADS Constructor
    def __init__( self, debug = False, options = None ):
        super( ServiceNAADS, self ).__init__( debug = debug, options = options )

        app_dir = self._configData["app_dir"]

        # Setup the Filter for Test Alerts...
        self.filterTestAlerts = False
        if "filterTestAlerts" in self._configData:
            self.filterTestAlerts = self._configData["filterTestAlerts"];
        # end if

        # Setup the Filter for Test Alerts...
        self.publishTestAlertsOnly = False
        if "publishTestAlertsOnly" in self._configData:
            self.publishTestAlertsOnly = self._configData["publishTestAlertsOnly"];
        # end if

        self._currentQueryTime = datetime.utcnow()
    # end __init__()

    # _getExternalData()
    #
    # Overloaded from the base class.
    # Get the data from the external feed and return it in any format to be
    # parsed by _processExternalData()
    #
    # @return Object - All the data to be parsed.
    def _getExternalData( self ):
        # Build the url we will need...
        url = str( self._configData["feed_url"] )

        feed = None
        feedText = None

        headers = {
            'user-agent': 'MASAS-Watcher'
        }

        try:
            feed = requests.get( url, headers=headers )
        except Exception, ex:
            self.logger.error( "Data request failed: " + ex.message )
        # end try

        if( feed is not None ):
            # We want the raw data of the feed for now...
            feedText = feed.content

            # Update the query time...
            self._currentQueryTime = datetime.utcnow()
        # end if

        return feedText
    # end _getExternalData()

    # _processExternalData()
    #
    # Overloaded from the base class.
    # From the "externalData" object, extract the individual items.
    # Return the objects as an array to be parsed by the
    # _processExternalDataItem method.
    #
    # @input externalData Object - An Object containing all the items to be extracted.
    # @return Array - The extracted items.
    def _processExternalData( self, externalData ):
        feedItems = {}

        # Get the the time of the last query...
        lastQueryDT = self._serviceDataMgr.serviceData.lastQueryDateTime

        # Get all the entries...
        root = ET.fromstring( externalData )
        elEntries = root.findall( ".//{http://www.w3.org/2005/Atom}entry" )

        self.logger.info( str( len( elEntries ) ) + " entries have been retrieved." )

        # Filter out the entries that have been previously processed...
        for elEntry in elEntries:
            elUpdated = elEntry.find( ".//{http://www.w3.org/2005/Atom}updated" )
            updatedDT = dateutil.parser.parse( elUpdated.text, ignoretz=True )

            if( updatedDT >= lastQueryDT ):
                # Since, the languages each have their own entries, we need to group them together...
                elIdentifier = elEntry.find( ".//{http://www.w3.org/2005/Atom}id" )
                id = elIdentifier.text

                # Get the Status value out of the Entry to be filtered later...
                status = "Actual"
                elCategories = elEntry.findall( ".//{http://www.w3.org/2005/Atom}category" )
                for elCategory in elCategories:
                    termValue = elCategory.get( "term" )
                    if( termValue.find( "status" ) != -1 ):
                        status = termValue.replace( "status=", "" )
                        break
                    # end if
                # end for

                # Track if we publish this item or not.
                publishItem = True

                # Is this item a "TEST"? Do we need to filter it out?
                if( self.filterTestAlerts == True and status == "Test" ):
                    publishItem = False
                    self.logger.info( "A TEST Alert has been filtered: " + id )
                elif( self.publishTestAlertsOnly == True and status != "Test" ):
                    publishItem = False
                    self.logger.info( "A NON TEST Alert has been filtered: " + id )
                # endif

                if publishItem:
                    # The item is good, process it...
                    value = None
                    if( feedItems.has_key( id ) ):
                        value = feedItems[id]
                    else:
                        value = FeedItem( id )
                        # Find the link to the Alert...
                        elLinks = elEntry.findall( ".//{http://www.w3.org/2005/Atom}link" )
                        for elLink in elLinks:
                            if( elLink.get( "rel" ) == "alternate" ):
                                value.url = elLink.get( "href" )
                                break
                            # end if
                        # end for
                    # end if

                    value.entries.append( elEntry )
                    feedItems[id] = value
                # end if
            # end if
        # end for

        # Now we can update the last query DT...
        self._serviceDataMgr.serviceData.lastQueryDateTime = self._currentQueryTime

        self.logger.info( str( len( feedItems ) ) + " entries have been marked as updated." )

        return feedItems.values()
    # end _processExternalData()

    # _processExternalDataItem()
    #
    # Overloaded from the base class.
    # From the "dataItem" object, extract the contents and populate
    # a AlertData or EntryData with the necessary fields.
    #
    # @input dataItem Object - A a single item containing an Entry/Alert.
    # @return BaseDataItem - The Entry/Alert to be published.
    def _processExternalDataItem( self, dataItem ):
        # Create the business object...
        masasItem = AlertData()

        url = dataItem.url

        # Get the Alert...
        # We got the notification, but the information may not be available right away...
        numOfRetries = 0

        while numOfRetries < 31:
            root = None

            headers = {
                'user-agent': 'MASAS-Watcher'
            }

            requestData = requests.get( url, headers=headers )

            if( requestData.status_code == 200 ):

                # Parse the data into a valid alert...
                alert_raw = requestData.content

                try:
                    root = ET.XML( alert_raw )
                    # if all is good, break the loop...
                    break
                except Exception, ex:
                    self.logger.error( "Alert parsing failed: " + ex.message )
            else:
                self.logger.error( "Data request (url=" + url + ") failed.  Status code:" + str( requestData.status_code ) )

            numOfRetries += 1
            time.sleep( 5 )
        # end while

        # The whole requests has failed...
        if( root is None ):
            return None

        # Get the Identifier
        alertIdEl = root.find(".//cap:identifier", {"cap" : "urn:oasis:names:tc:emergency:cap:1.2"} )
        alertId = alertIdEl.text
        alertRefEl = root.find(".//cap:references", {"cap" : "urn:oasis:names:tc:emergency:cap:1.2"} )
        alertReferences = ""
        if( alertRefEl is not None ):
            alertReferences = alertRefEl.text
        # end if

        # Has this alert been created already?
        masasItemId = self._serviceDataMgr.findEntryIdBySourceId( alertId )

        if( masasItemId is not None ):
            # The item has already been created, most likely from another source...
            return None
        # end if

        # Get the references
        if masasItemId is None and alertReferences is not None:
            # Get the IDs from the references
            referenceIdentifiers = self._getReferencesIdentifiers( alertReferences )
            for referenceId in referenceIdentifiers:
                masasItemId = self._serviceDataMgr.findEntryIdBySourceId( referenceId )
                if masasItemId is not None:
                    # We have a previously published item!
                    break
            # end for
        # end if

        # Remove the signature for now...
        signatures = root.findall( '{http://www.w3.org/2000/09/xmldsig#}Signature' )
        if signatures is not None:
            for signature in signatures:
                root.remove( signature )
            # end for
        # end if

        # Remove the derefURIs...
        derefURIParents = root.findall( ".//cap:derefUri/..", {"cap" : "urn:oasis:names:tc:emergency:cap:1.2"} )
        if derefURIParents is not None:
            for derefURIParent in derefURIParents:
                derefURIs = derefURIParent.findall( ".//cap:derefUri", {"cap" : "urn:oasis:names:tc:emergency:cap:1.2"} )
                for derefUri in derefURIs:
                     #derefUri.parentNode().remove(derefUri)
                    derefURIParent.remove( derefUri )
                # end for
            # end if
        # end if

        masasItem.masasIdentifier = masasItemId
        masasItem.sourceIdentifier = alertId
        masasItem.sourceData = ET.tostring( root )

        # Set the data to publish...
        masasItem.data = masasItem.sourceData
        
        # Done!
        return masasItem
    # end _processExternalDataItem()

    def _getReferencesIdentifiers(self, referencesStr ):
        referencesIdentifiers = []

        if not len( referencesStr ) == 0:
            referencesList = str( referencesStr ).split( ' ' )
            for referenceStr in referencesList:
                reference = referenceStr.split( ',' )
                referencesIdentifiers.append( reference[1] )
            # end for

        return referencesIdentifiers
    # end _getReferencesIdentifiers

# end ServiceNAADS

if __name__ == '__main__':

    usage = "usage: service_NAADS [-d] [-m] [-c]"

    parser = optparse.OptionParser(usage)
    parser.add_option("-d", action="store_true", dest="debug",
                      help="print debugging messages")
    parser.add_option("-m", action="store_true", dest="monitor",
                      help="run as a monitoring daemon")
    parser.add_option("-c", action="store", type="string", dest="configFileLocation",
                      help="Config file location.")
    (options, args) = parser.parse_args()

    service = None
    try:
        service = ServiceNAADS( debug = options.debug, options = options )
    except Exception, ex:
        service.close()
        logging.error( "An unhandled error occurred during the creation of the Service: " + ex.message )
        sys.exit()
    # end try

    # TODO - MOVE THIS INTO THE BASE WATCHER CLASS...
    if( options.debug ):
        service.run()
    else:
        try:
            service.run()
        except Exception, ex:
            service.close()
            logging.error( "An unhandled error occurred in the Service: " + ex.message )
            sys.exit()
        # end try
    # end if

    # run in monitoring mode if requested
    if options.monitor:
        min_int = int( service.check_interval )
        sec_int = min_int * 60
        service.logger.debug( "Monitor mode: updating every %d minutes" %min_int )
        while 1:
            time.sleep( sec_int )

            if( options.debug ):
                service.run()
            else:
                try:
                    service.run()
                except Exception, ex:
                    service.close()
                    logging.error( "An unhandled error occurred in the Service: " + ex.message )
                    sys.exit()
                # end try
            # end if

        # end while
    # end if

    if( service ):
        service.close()
    # end if

# end if
