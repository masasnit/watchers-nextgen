import json
import logging
import logging.config
import optparse
import os
import pika
import random
import requests
import string
import sys
import time
import xml.etree.ElementTree as ET

from datetime import datetime

from EMPSUtils.MASAS.MASASClient import MASASClient

from watchers.core.AlertData import AlertData
from watchers.core.EntryData import EntryData
from watchers.core.ServiceDataManager import ServiceDataManager


class ServiceEC( object ):

    # ServiceEC Constructor
    def __init__( self, debug = False, options = None  ):

        # Setup internal support variables...
        self._debug = debug                                 # Debug messages turned on/off...
        self._config_data_file = "config.json"              # Set the name of the config data file..
        self._configData = None                             # Init the config data object.

        # Setup the file paths...
        self._defaultPath = "."
        self._systemSettingPath = "."

        dirName = os.path.dirname( sys.argv[0] )
        topLevelName = os.path.split( dirName )[1]
        self._systemSettingPath = os.path.join( "/etc/MASAS/watchers", topLevelName )

        # Setup the service logging...
        self._setup_logging()

        # Config system file path...
        config_user_file_path = os.path.join( self._systemSettingPath, self._config_data_file )

        # Config default file path...
        config_default_file_path = os.path.join( self._defaultPath, self._config_data_file )

        # Open/Read the config file...

        # Try the system file path...
        if( os.path.isfile( config_user_file_path ) ):
            self.logger.info( 'Watcher configuration file found: ' + config_user_file_path )
            with open( config_user_file_path ) as configFile:
                self._configData = json.load( configFile )
            # end with
        # end if

        if( ( self._configData is None ) and ( os.path.isfile( config_default_file_path ) ) ):
            self.logger.info( 'Watcher configuration file found: ' + config_user_file_path )
            # Try the default file path...
            with open( config_default_file_path ) as configFile:
                self._configData = json.load( configFile )
            # end with
        # end if

        if self._configData is None:
            raise RuntimeError( "No configuration data." )
        # end if

        # Setup internal support variables based on the configuration file...
        self._watch_name = self._configData["watch_name"]           # User defined Watch name
        if self._watch_name is None:
            self._watch_name = __name__                             # Default to the module name

        self._app_dir = self._configData["app_dir"]                 # Current application directory

        # Setup member variables based on the configuration file...
        self.check_interval = self._configData["check_interval"]    # Run interval value

        # Load/Initialize the previous run's data...
        self._serviceDataMgr = ServiceDataManager( self, self._watch_name,
                                                   host = self._configData["host"], port = self._configData["port"],
                                                   database = self._configData["database"],
                                                   user = self._configData["user"],password = self._configData["password"] )

        app_dir = self._configData["app_dir"]

        # Create the MASAS Client...
        url = self._configData["masas_hub_url"]
        secret = self._configData["masas_hub_secret"]
        self.masas_client = MASASClient( url, secret )

        # Load the event codes...
        self._eventCodes = {}
        self._eventcodes_data_file = "eventcodes.json"              # Set the name of the config data file..
        eventcodes_file_path = os.path.join( "..//data", self._eventcodes_data_file )
        with open( eventcodes_file_path ) as eventcodesFile:
            self._eventCodes = json.load( eventcodesFile )
        # end with

        # Setup the RabbitMQ variables...
        self._connection = None
        self._channel = None
        self._closing = False
        self._consumer_tag = None

        self._amqpData = self._configData["amqp"]
        self._amqp_host     = self._amqpData.get( "host", "dd.weather.gc.ca" )
        self._amqp_port     = self._amqpData.get( "port", "5672" )
        self._amqp_user     = self._amqpData.get( "user", "anonymous" )
        self._amqp_passwd   = self._amqpData.get( "password", "anonymous" )

        self._exchange      = self._amqpData.get( "exchange", "xpublic" )
        self._exchange_type = self._amqpData.get( "exchange_type", "topic" )
        self._exchange_key  = self._amqpData.get( "exchange_key", [] )

        # TODO!
        self._queue_name = None
        self._init_queue_name()

        self._url = "amqp://%s:%s@%s:%s/" % ( self._amqp_user, self._amqp_passwd, self._amqp_host, self._amqp_port ) + "%2F"

        # Current time...
        self._currentQueryTime = datetime.utcnow()
    # end __init__()

    def _init_queue_name( self ):

        queueFile = os.path.join( "./", "queue_name.cfg" )

        # Does the config file already exist?
        if( os.path.isfile( queueFile ) ):
            # Load the previously generated queue name...
            f = open( queueFile )
            self._queue_name = f.read()
            f.close()
        else :
            # Generate a new queue name...
            self._queue_name  = 'cmc'
            self._queue_name += '.' + string.zfill( random.randint( 0, 100000000 ), 8 )
            self._queue_name += '.' + string.zfill( random.randint( 0, 100000000 ), 8 )

            # Save it to the config file for future use...
            f = open( queueFile,'w' )
            f.write( self._queue_name )
            f.close()
        # end if
    # end _init_queue_name()

    def _setup_logging( self ):
        logging.info( 'Initializing logging...' )
        logConfigFileName = "logging.conf"
        logConfigSystemFilePath = os.path.join( self._systemSettingPath, logConfigFileName )
        logConfigDefaultFilePath = os.path.join( self._defaultPath, logConfigFileName )

        validLogConfigPath = logConfigDefaultFilePath

        # Try the system file path...
        if( os.path.isfile( logConfigSystemFilePath ) ):
            validLogConfigPath = logConfigSystemFilePath
        # end if

        logging.config.fileConfig( validLogConfigPath )
        self.logger = logging.getLogger( 'watcherService' )

        self.logger.info( 'Logging configuration file found: ' + validLogConfigPath )
    # end _setup_logging(self):

    def start( self ):
        # Run the message queue...
        self._connection = self._connect()
        self._connection.ioloop.start()
    # end start()

    def stop( self ):
        self._closing = True

        if self._channel:
            self.logger.debug( 'Sending a Basic.Cancel RPC command to RabbitMQ' )
            self._channel.basic_cancel( self._on_cancelok, self._consumer_tag )

        # Wait until it's properly closed...
        self._connection.ioloop.start()
    # end stop()

    def _connect( self ):
        self.logger.info( 'Connecting to %s', self._url )
        return pika.SelectConnection( pika.URLParameters( self._url ), self._on_connection_open, stop_ioloop_on_close=False )
    # end _connect()

    def _reconnect( self ):
        # This is the old connection IOLoop instance, stop its ioloop
        self._connection.ioloop.stop()

        if not self._closing:
            # Create a new connection
            self._connection = self._connect()

            # There is now a new connection, needs a new ioloop to run
            self._connection.ioloop.start()
        # end if
    # end _reconnect()

    def _close_channel( self ):
        self.logger.debug( 'Closing the channel' )
        self._channel.close()
    # end _close_channel()

    def _on_cancelok( self, unused_frame ):
        self.logger.info( 'RabbitMQ acknowledged the cancellation of the consumer' )
        self._close_channel()
    # end _on_cancelok()

    def _on_connection_open( self, unused_connection ):
        self.logger.debug( 'Connection opened' )
        self._connection.add_on_close_callback( self._on_connection_closed )
        self._connection.channel( on_open_callback=self._on_channel_open )
    # end _on_connection_open()

    def _on_connection_closed( self, connection, reply_code, reply_text ):
        self._channel = None
        if self._closing:
            self._connection.ioloop.stop()
        else:
            self.logger.warning( 'Connection closed, reopening in 5 seconds: (%s) %s', reply_code, reply_text )
            self._connection.add_timeout( 5, self._reconnect )
    # end _on_connection_closed()

    def _on_channel_open( self, channel ):
        self.logger.debug('Channel opened')
        self._channel = channel
        self._channel.add_on_close_callback( self._on_channel_closed )

        # Setup the exchange...
        self.logger.info( 'Declaring exchange %s', self._exchange )
        self._channel.exchange_declare( self._on_exchange_declareok, self._exchange, self._exchange_type,
                                        passive=False, durable=False, auto_delete=False, internal=False, nowait=False,
                                        arguments=None )
    # end _on_channel_open()

    def _on_channel_closed(self, channel, reply_code, reply_text):
        self.logger.warning( 'Channel %i was closed: (%s) %s', channel, reply_code, reply_text )
        self._connection.close()
    # end _on_channel_closed()

    def _on_exchange_declareok( self, unused_frame ):
        self.logger.debug( 'Exchange declared' )
        self.logger.info('Declaring queue %s', self._queue_name)
        self._channel.queue_declare(self._on_queue_declareok, self._queue_name, passive=False, durable=False,
                      exclusive=False, auto_delete=False, nowait=False,
                      arguments={'x-expires' : 1800000})
    # end _on_exchange_declareok()

    def _on_queue_declareok( self, method_frame ):
        for k in self._exchange_key[:-1]:
            self.logger.info( 'Binding %s to %s with %s', self._exchange, self._queue_name, k )
            self._channel.queue_bind( None, self._queue_name, self._exchange, k )
        # end for

        self.logger.info( 'Binding %s to %s with %s', self._exchange, self._queue_name, self._exchange_key[-1] )
        self._channel.queue_bind( self._on_bindok, self._queue_name, self._exchange, self._exchange_key[-1] )
    # end _on_queue_declareok()

    def _on_bindok( self, unused_frame ):
        self.logger.debug( 'Queue bound' )
        self.logger.debug( 'Issuing consumer related RPC commands' )
        self.logger.debug( 'Adding consumer cancellation callback' )
        self._channel.add_on_cancel_callback( self._on_consumer_cancelled )

        # this line permits parallel pull
        self._channel.basic_qos( prefetch_count=1 )
        self._consumer_tag = self._channel.basic_consume( self._on_message, self._queue_name )
    # end _on_bindok()

    def _on_consumer_cancelled( self, method_frame ):
        self.logger.info( 'Consumer was cancelled remotely, shutting down: %r', method_frame )
        if self._channel:
            self._channel.close()
    # end _on_consumer_cancelled()

    def _on_message( self, unused_channel, basic_deliver, properties, body ):
        self.logger.debug( 'Received message # %s: %s', basic_deliver.delivery_tag, body )

        # Update the query time...
        self._currentQueryTime = datetime.utcnow()

        # Process the data...
        self._processData( body )
        self.logger.debug( 'Acknowledging message %s', basic_deliver.delivery_tag )
        self._channel.basic_ack( basic_deliver.delivery_tag )

    # end _on_message()

    def _processData( self, data ):
        # process the item...
        errorOccurred = False

        masas_entry_identifier = None
        dataItem = self._processExternalDataItem( data )

        if dataItem is not None:
            isNewItem = dataItem.masasIdentifier is None

            # todo - Clean this up to be a retry loop that calls "Publish".  In the Publish method check to see if it's new and an entry/alert...
            if isinstance( dataItem, EntryData ):
                if isNewItem is True:
                    masas_entry_identifier = self.masas_client.add_entry( dataItem.data )
                    self.logger.debug( "Added an Entry! (MASAS_ENTRY_ID=" + masas_entry_identifier + ")" )
                else:
                    masas_entry_identifier = self.masas_client.update_entry( dataItem.masasIdentifier, dataItem.data )
                    self.logger.debug( "Updated an Entry! (MASAS_ENTRY_ID=" + masas_entry_identifier + ")" )
                # end if
            elif isinstance( dataItem, AlertData ):
                if isNewItem is True:
                    try:
                        masas_entry_identifier = self.masas_client.add_alert_XML( dataItem.data )
                        self.logger.debug( "Added an Alert! (MASAS_ENTRY_ID=" + masas_entry_identifier + ")" )
                    except ValueError as ex:
                        self.logger.error( "An occurred during in masas_client.add_alert_XML(): " + ex.message )
                        self.logger.debug( "Data content:" )
                        self.logger.debug( dataItem.data )
                        #if "Duplicate content entry" in ex.message:
                            # The alert has been pushed in already... Have to see what we can do with this!
                        errorOccurred = True
                        # end if
                    # end try
                else:
                    try:
                        masas_entry_identifier = self.masas_client.update_alert_XML( dataItem.masasIdentifier, dataItem.data )
                        self.logger.debug( "Updated an Alert! (MASAS_ENTRY_ID=" + masas_entry_identifier + ")" )
                    except ValueError as ex:
                        self.logger.error( "An occurred during in masas_client.add_alert_XML(): " + ex.message )
                        self.logger.debug( "Data content:" )
                        self.logger.debug( dataItem.data )
                        errorOccurred = True
                    # end try
                # end if
            # end if

            dataItem.masasIdentifier = masas_entry_identifier

            # if no error occurred
            if not errorOccurred:
                # Save the item for the next run...
                self._serviceDataMgr.savePublishedItem( dataItem )
            #end if

        # end if

        # Save the Service Data...
        self._serviceDataMgr.save()

        return not errorOccurred
    #end run()

    # _processExternalDataItem()
    #
    # Overloaded from the base class.
    # From the "dataItem" object, extract the contents and populate
    # a AlertData or EntryData with the necessary fields.
    #
    # @input dataItem Object - A a single item containing an Entry/Alert.
    # @return BaseDataItem - The Entry/Alert to be published.
    def _processExternalDataItem( self, dataItem ):
        # Create the business object...
        masasItem = AlertData()

        url = dataItem

        # Get the Alert...
        # We got the notification, but the information may not be available right away...
        numOfRetries = 0

        while numOfRetries < 31:
            root = None
            requestData = requests.get( url )

            if( requestData.status_code == 200 ):

                # Parse the data into a valid alert...
                alert_raw = requestData.content

                try:
                    root = ET.XML( alert_raw )
                    # if all is good, break the loop...
                    break
                except Exception, ex:
                    self.logger.error( "Alert parsing failed: " + ex.message )
            else:
                self.logger.error( "Data request (url=" + url + ") failed.  Status code:" + str( requestData.status_code ) )

            numOfRetries += 1
            time.sleep( 5 )
        # end while

        # The whole requests has failed...
        if( root is None ):
            return None

        # Get the Identifier
        alertIdEl = root.find(".//cap:identifier", {"cap" : "urn:oasis:names:tc:emergency:cap:1.2"} )
        alertId = alertIdEl.text
        alertRefEl = root.find(".//cap:references", {"cap" : "urn:oasis:names:tc:emergency:cap:1.2"} )
        alertReferences = alertRefEl.text

        # Has this alert been created already?
        masasItemId = self._serviceDataMgr.findEntryIdBySourceId( alertId )

        # Get the references
        if masasItemId is None and alertReferences is not None:
            # Get the IDs from the references
            referenceIdentifiers = self._getReferencesIdentifiers( alertReferences )
            for referenceId in referenceIdentifiers:
                masasItemId = self._serviceDataMgr.findEntryIdBySourceId( referenceId )
                if masasItemId is not None:
                    # We have a previously published item!
                    break
            # end for
        # end if

        masasItem.masasIdentifier = masasItemId
        masasItem.sourceIdentifier = alertId
        masasItem.sourceData = alert_raw.decode( 'utf8' )

        # Set the data to publish...
        masasItem.data = alert_raw

        # Done!
        return masasItem

    def _getReferencesIdentifiers(self, referencesStr ):
        referencesIdentifiers = []

        if not len( referencesStr ) == 0:
            referencesList = str( referencesStr ).split( ' ' )
            for referenceStr in referencesList:
                reference = referenceStr.split( ',' )
                referencesIdentifiers.append( reference[1] )
            # end for

        return referencesIdentifiers
    # end _getReferencesIdentifiers

# end ServiceEC

if __name__ == '__main__':

    usage = "usage: Service_EC [-d] [-c]"

    parser = optparse.OptionParser(usage)
    parser.add_option("-d", action="store_true", dest="debug",
                      help="print debugging messages")
    parser.add_option("-c", action="store", type="string", dest="configFileLocation",
                      help="Config file location.")
    (options, args) = parser.parse_args()

    service = None
    try:
        service = ServiceEC( debug = options.debug, options = options )
    except Exception, ex:
        service.close()
        logging.error( "An unhandled error occurred during the creation of the Service: " + ex.message )
        sys.exit()
    # end try

    if( options.debug ):
        service.start()
    else:
        while( True ):
            try:
                service.start()
            except KeyboardInterrupt:
                logging.info( "Closing the connection." )
                service.stop()
                service.close()
                break
            except Exception, ex:
                logging.error( "An unhandled error occurred in the Service: " + ex.message )
                service.close()
                time.sleep( 10 )
            # end try
        # end while
    # end if

    if( service ):
        service.close()
    # end if

    logging.info( "Service shutdown." )
# end if