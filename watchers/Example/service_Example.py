import logging
import optparse
import time
import sys
import json
from datetime import datetime, timedelta

import requests
from geojson import Point

from watchers.core.EntryData import EntryData
from watchers.core.WatcherService import WatcherService
from EMPSUtils.MASAS.Entry.Entry import Entry
from EMPSUtils.MASAS.Entry.Entry import CertaintyEnum


class ServiceExample( WatcherService ):

    # ServiceExample Constructor
    def __init__( self, debug = False, options = None  ):
        super( ServiceExample, self ).__init__( debug = debug, options = options )

        app_dir = self._configData["app_dir"]
    # end __init__()

    # _getExternalData()
    #
    # Overloaded from the base class.
    # Get the data from the external feed and return it in any format to be
    # parsed by _processExternalData()
    #
    # @return Object - All the data to be parsed.
    def _getExternalData( self ):
        # Get the the time of the last query...
        lastQueryDT = self._serviceDataMgr.serviceData.lastQueryDateTime

        # Build the url we will need...
        url = str( self._configData["feed_url"] )

        feed = None
        feedText = None

        try:
            feed = requests.get( url )
        except Exception, ex:
            self.logger.error( "Data request failed: " + str( ex.message ) )
        # end try

        if( feed is not None ):
            # We want the raw data of the feed for now...
            feedText = feed.text

            # Update the query time...
            self._serviceDataMgr.serviceData.lastQueryDateTime = datetime.utcnow()
        # end if

        return feedText
    # end _getExternalData()

    # _processExternalData()
    #
    # Overloaded from the base class.
    # From the "externalData" object, extract the individual items.
    # Return the objects as an array to be parsed by the
    # _processExternalDataItem method.
    #
    # @input externalData Object - An Object containing all the items to be extracted.
    # @return Array - The extracted items.
    def _processExternalData( self, externalData ):
        feedItems = []

        root = json.loads( externalData )

        if( "feed_data" in root ):
            feedData = root["feed_data"]
            for feedItem in feedData:
                feedItems.append( feedItem )
            # end for
        # end if

        return feedItems
    # end _processExternalData()

    # _processExternalDataItem()
    #
    # Overloaded from the base class.
    # From the "dataItem" object, extract the contents and populate
    # a AlertData or EntryData with the necessary fields.
    #
    # @input dataItem Object - A a single item containing an Entry/Alert.
    # @return BaseDataItem - The Entry/Alert to be published.
    def _processExternalDataItem( self, dataItem ):
        # Create the business object...
        masasItem = EntryData()

        # Have we seen this item before? If so, we need the associated Entry ID...
        masasItem.masasIdentifier = self._serviceDataMgr.findEntryIdBySourceId( dataItem["id"] )

        # Create the Entry...
        entry = Entry()

        # Populate the Entry with data...
        entry.setTitle( dataItem["title"] )
        entry.setContent( dataItem["description"] )
        entry.geometry = Point( ( dataItem["latitude"], dataItem["longitude"] ) )
        entry.expires = datetime.utcnow() + timedelta( minutes=30 )

        entry.certainties.append( CertaintyEnum.CertaintyUnknown )

        # Set the source identifier
        masasItem.sourceIdentifier = dataItem["id"]

        # Set the source data for future lookups (saved to DB)
        masasItem.sourceData = str( dataItem )

        # Set the data to publish...
        masasItem.data = entry

        # Done!
        return masasItem
    # end _processExternalDataItem()

# end ServiceExample

if __name__ == '__main__':

    usage = "usage: service_Example [-d] [-m] [-c]"

    parser = optparse.OptionParser(usage)
    parser.add_option("-d", action="store_true", dest="debug",
                      help="print debugging messages")
    parser.add_option("-m", action="store_true", dest="monitor",
                      help="run as a monitoring daemon")
    parser.add_option("-c", action="store", type="string", dest="configFileLocation",
                      help="Config file location.")
    (options, args) = parser.parse_args()

    service = None
    try:
        service = ServiceExample( debug = options.debug, options = options )
    except Exception, ex:
        service.close()
        logging.error( "An unhandled error occurred during the creation of the Service: " + ex.message )
        sys.exit()
    # end try

    # TODO - MOVE THIS INTO THE BASE WATCHER CLASS...
    if( options.debug ):
        service.run()
    else:
        try:
            service.run()
        except Exception, ex:
            service.close()
            logging.error( "An unhandled error occurred in the Service: " + ex.message )
            sys.exit()
        # end try
    # end if

    # run in monitoring mode if requested
    if options.monitor:
        min_int = int( service.check_interval )
        sec_int = min_int * 60
        service.logger.debug( "Monitor mode: updating every %d minutes" %min_int )
        while 1:
            time.sleep( sec_int )

            if( options.debug ):
                service.run()
            else:
                try:
                    service.run()
                except Exception, ex:
                    service.close()
                    logging.error( "An unhandled error occurred in the Service: " + ex.message )
                    sys.exit()
                # end try
            # end if

        # end while
    # end if

    if( service ):
        service.close()
    # end if

# end if