import logging
import optparse
import time
import requests
import sys
import json
import geojson
from itertools import izip

import pytz
from pytz import timezone
from datetime import datetime, timedelta
from dateutil.parser import parse

import lxml.html
import xml.etree.ElementTree as ET

from EMPSUtils.MASAS.Entry.Entry import Entry
from watchers.core.WatcherService import WatcherService
from watchers.core.EntryData import EntryData

class ServiceBCHydro( WatcherService ):

    def __init__( self, debug = False, options = None  ):
        super( ServiceBCHydro, self ).__init__( debug = debug, options = options )

        app_dir = self._configData["app_dir"]

    # end __init__()

    def _getExternalData( self ):
        lastQueryDT = self._serviceDataMgr.serviceData.lastQueryDateTime
        lastQueryDTFormatted = lastQueryDT.strftime( "%Y-%m-%dT%H:%M:%SZ")

        url = str( self._configData["feed_url"] )
        currentQueryDT = datetime.utcnow()

        feed = None
        feedText = None
        try:
            feed = requests.post( url )
        except Exception, ex:
            self.logger.error( "Data request failed: " + ex.message )
        # end try

        if( feed is not None ):
            feedText = feed.text
            self._serviceDataMgr.serviceData.lastQueryDateTime = currentQueryDT
        # end if

        return feedText
    # end _getExternalData()

    def _processExternalData( self, externalData ):
        entries = []

        # Load JSON objects
        root = json.loads(externalData)
        for r in root["outages"]:
            entries.append(r)

        return entries
    # end _processExternalData()

    def _processExternalDataItem( self, dataItem ):
        masasItem = None

        # Get the description and parse the table into json...
        newDescription = str(dataItem['description'])
        itemValues = self._getNameValues( newDescription )

        # Don't process the data by default...
        processData = True

        # Get the source item id...
        id = str( dataItem[ 'id' ] )

        # Get the timestamp from the data item...
        description = newDescription

        # Get the last updated DateTime in the appropriate time zone
        lastUpdatedStr = itemValues['Last Updated']
        updatedDT = self._convertTime( lastUpdatedStr, timezone('America/Vancouver') )

        # Is this an update???
        entryInfo = self._serviceDataMgr.findEntryBySourceId( id )

        if( entryInfo != None ):
            # We need the entry update time in UTC...
            entryUpdateTime = self._convertTime(entryInfo['updated'], timezone('America/Toronto'))

            # Don't update the data unless it's new...
            if( updatedDT <= entryUpdateTime ):
                processData = False
            # end if

        # end if

        if( processData == True):

            # Create the business object...
            masasItem = EntryData()

            # Have we seen this item before? If so, we need the associated Entry ID...
            if( entryInfo != None ):
                masasItem.masasIdentifier = entryInfo['entryId']

            # Create the Entry...
            entry = Entry()

            #Add Categories
            entry.categories.append("Infra")
            entry.status = "Actual"
            entry.icon = "ems/infrastructure/energy"

            #Add content block
            title = 'Hydro Outage - ' + itemValues['Cause']
            content = 'Crew Status: ' + itemValues['Crew Status'] + "\n"
            content += 'Time Off: ' + itemValues['Time Off'] + "\n"
            content += 'Cause: ' + itemValues['Cause'] + "\n"
            content += '# Customers Affected: ' + itemValues['# Customers Affected'] + "\n"
            content += 'Last Updated: ' + itemValues['Last Updated'] + "\n"

            # Let's figure out the expiry time...
            expiryString = None
            if( itemValues.has_key('Est. Arrival') ):
                expiryString = itemValues['Est. Arrival']
                content += 'Est. Arrival: ' + itemValues['Est. Arrival'] + "\n"
            elif (itemValues.has_key('Est. Time On')):
                expiryString = itemValues['Est.Time On']
                content += 'Est. Time On: ' + itemValues['Est.Time On'] + "\n"
            # end if

            try:
                entry.expires = self._convertTime(expiryString, timezone('America/Vancouver')) + timedelta(hours=6)
            except Exception, ex:
                # Chances are there's an non date/time string here...
                entry.expires = datetime.utcnow() + timedelta(hours=6)
            # end try

            entry.setContent(content)
            entry.setTitle(title)

            geometry = self.formatGeometry( dataItem['polygon'] )
            entry.geometry = geojson.loads( geometry )

            # Set the source identifier
            masasItem.sourceIdentifier = id

            # Set the source data for future lookups (saved to DB)
            # TODO: this needs to be added!
            masasItem.sourceData = str( dataItem )
            # Set the data to publish...
            masasItem.data = entry
            return masasItem
            #return None
        # end if

        return None
    # end _processExternalDataItem()

    def _convertTime(self, timeValue, timesStringTZ):
        timeObject = timeValue

        if( isinstance( timeValue, basestring ) ):
            timeObject = parse(timeValue)

        timeObject = timesStringTZ.localize(timeObject)
        timeObject = timeObject.astimezone(pytz.utc)

        return timeObject
    # end _convertTime

    def _getNameValues(self, description):
        nameValues = {}

        # Parse the HTML...
        htmlDoc = lxml.html.fromstring( description )

        # Get all the table rows...
        xpathQuery = "//table//tbody//tr"
        tableRows = htmlDoc.xpath(xpathQuery)

        for row in tableRows:
            #  "".join(c.itertext()) -> Join all the inner text of element C (and it's children)
            # For each child element (table cell) get the inner text and assign to values[x]...
            values = [ "".join(c.itertext()) for c in row.getchildren() ]

            if( len( values ) > 1 ):
                key = values[0]
                key = key.replace( ":","" )
                key = key.strip()

                nameValues[key] = values[1]
            # end if
        # end for

        return nameValues
    # end _getNameValues()
    
    def pairwise(self, iterable):
        "s -> (s0,s1), (s2,s3), (s4, s5), ..."
        a = iter(iterable)
        return izip(a, a)
    
    #Provide the gui of an item to find it's related geometry
    def formatGeometry( self, polygonCoords ):
        geojsonString = '{"coordinates":[[['
        for p1,p2 in self.pairwise( polygonCoords ):
            geojsonString += str(p2) + "," + str(p1) + "],["

        geojsonString += str( polygonCoords[1] ) + "," + str( polygonCoords[0] ) + "],["
        geojsonString = geojsonString[:-2]
        geojsonString += ']], "type": "MultiPolygon"}'
        return geojsonString
    # end formatGeometry()


# end ServiceBCHydro

if __name__ == '__main__':

    usage = "usage: service_BCHydro [-d] [-m] [-c]"

    parser = optparse.OptionParser(usage)
    parser.add_option("-d", action="store_true", dest="debug",
                      help="print debugging messages")
    parser.add_option("-m", action="store_true", dest="monitor",
                      help="run as a monitoring daemon")
    parser.add_option("-c", action="store", type="string", dest="configFileLocation",
                      help="Config file location.")
    (options, args) = parser.parse_args()

    service = None
    try:
        service = ServiceBCHydro( debug = options.debug, options = options )
    except Exception, ex:
        service.close()
        logging.error( "An unhandled error occurred during the creation of the Service: " + ex.message )
        sys.exit()
    # end try

    # TODO - MOVE THIS INTO THE BASE WATCHER CLASS...
    if( options.debug ):
        service.run()
    else:
        try:
            service.run()
        except Exception, ex:
            service.close()
            logging.error( "An unhandled error occurred in the Service: " + ex.message )
            sys.exit()
        # end try
    # end if

    # run in monitoring mode if requested
    if options.monitor:
        min_int = int( service.check_interval )
        sec_int = min_int * 60
        service.logger.debug( "Monitor mode: updating every %d minutes" %min_int )
        while 1:
            time.sleep( sec_int )

            if( options.debug ):
                service.run()
            else:
                try:
                    service.run()
                except Exception, ex:
                    service.close()
                    logging.error( "An unhandled error occurred in the Service: " + ex.message )
                    sys.exit()
                # end try
            # end if

        # end while
    # end if

    if( service ):
        service.close()
    # end if

# end if