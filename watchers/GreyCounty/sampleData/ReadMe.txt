To use the sampleData.json, simply run a Python SimpleServer.

 1. Open a terminal
 2. Go into the folder containing sampleData.json
 3. run the cmd:
    > python -m SimpleHTTPServer 8000

You should now be able to access the file with this url:

    http://localhost:8000/sampleData.json

