import hashlib
import json
import logging
import optparse
import time
import requests
import sys
import json
from geojson import Point, LineString, Polygon, MultiPolygon

from datetime import datetime

from EMPSUtils.MASAS.Entry.Entry import Entry
from watchers.core.WatcherService import WatcherService
from watchers.core.EntryData import EntryData


class ServiceGreyCounty( WatcherService ):

    def __init__( self, debug = False, options = None  ):
        super( ServiceGreyCounty, self ).__init__( debug = debug, options = options )

        app_dir = self._configData["app_dir"]
    # end __init__()

    def _getExternalData( self ):
        lastQueryDT = self._serviceDataMgr.serviceData.lastQueryDateTime
        lastQueryDTFormatted = lastQueryDT.strftime( "%Y-%m-%dT%H:%M:%SZ")

        url = str( self._configData["feed_url"] )
        currentQueryDT = datetime.utcnow()

        feed = None
        feedText = None
        try:
            feed = requests.get( url )
            feed.raise_for_status()
        except Exception, ex:
            self.logger.error( ex.message )
            self.logger.error( "NOTIFY DEVS that URL is not available: %s" %url)
        # end try

        if( feed is not None ):
            feedText = feed.text
            self._serviceDataMgr.serviceData.lastQueryDateTime = currentQueryDT
        # end if

        return feedText
    # end _getExternalData()

    def _processExternalData( self, externalData ):
        entries = []
        #Load JSON objects
        root = json.loads(externalData)
        for r in root["features"]:
            entries.append(r)
        return entries
    # end _processExternalData()

    def _processExternalDataItem( self, dataItem ):
        masasItem = None

        # Don't process the data by default...
        processData = True

        # Create a hash from the item...
        strToHash = json.dumps( dataItem )
        m = hashlib.md5()
        m.update( strToHash )
        curDigest = m.hexdigest()
        #print hashValue

        entryInfo = self._serviceDataMgr.findEntryBySourceId( str(dataItem["attributes"]["RoadClosureID"]) )
        if( entryInfo != None and curDigest == entryInfo['digest'] ):
            # Don't update the data unless it's new...
            processData = False
        # end if

        if( processData == True):
            # Create the business object...
            masasItem = EntryData()

            # Set the digest...
            masasItem.digest = curDigest

            # Have we seen this item before? If so, we need the associated Entry ID...
            if( entryInfo != None ):
                masasItem.masasIdentifier = entryInfo['entryId']
            # end if

            # Create the Entry...
            entry = Entry()

            #Add Categories
            entry.categories.append("Transport")
            entry.status = "Actual"
            entry.icon = "ems/incident/roadway/roadwayClosure"
            #Add content block
            entry.setContent(dataItem["attributes"]["ClosureText"])
            #TODO: Add appropriate title for event
            entry.setTitle("Roadway Closure")


            entry.geometry = LineString(dataItem["geometry"]["paths"][0])
            # Set the source identifier
            masasItem.sourceIdentifier = dataItem["attributes"]["RoadClosureID"]

            # Set the source data for future lookups (saved to DB)
            masasItem.sourceData = str(dataItem)
            # Set the data to publish...
            masasItem.data = entry
        # end if

        return masasItem
    # end _processExternalDataItem()

# end ServiceGreyCounty

if __name__ == '__main__':

    usage = "usage: service_GreyCounty [-d] [-m] [-c]"

    parser = optparse.OptionParser(usage)
    parser.add_option("-d", action="store_true", dest="debug",
                      help="print debugging messages")
    parser.add_option("-m", action="store_true", dest="monitor",
                      help="run as a monitoring daemon")
    parser.add_option("-c", action="store", type="string", dest="configFileLocation",
                      help="Config file location.")
    (options, args) = parser.parse_args()

    service = None
    try:
        service = ServiceGreyCounty( debug = options.debug, options = options )
    except Exception, ex:
        service.close()
        logging.error( "An unhandled error occurred during the creation of the Service: " + ex.message )
        sys.exit()
    # end try

    # TODO - MOVE THIS INTO THE BASE WATCHER CLASS...
    if( options.debug ):
        service.run()
    else:
        try:
            service.run()
        except Exception, ex:
            service.close()
            logging.error( "An unhandled error occurred in the Service: " + ex.message )
            sys.exit()
        # end try
    # end if

    # run in monitoring mode if requested
    if options.monitor:
        min_int = int( service.check_interval )
        sec_int = min_int * 60
        service.logger.debug( "Monitor mode: updating every %d minutes" %min_int )
        while 1:
            time.sleep( sec_int )

            if( options.debug ):
                service.run()
            else:
                try:
                    service.run()
                except Exception, ex:
                    service.close()
                    logging.error( "An unhandled error occurred in the Service: " + ex.message )
                    sys.exit()
                # end try
            # end if

        # end while
    # end if

    if( service ):
        service.close()
    # end if

# end if