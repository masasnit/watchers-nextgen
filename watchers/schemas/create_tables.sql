﻿-- Database Table: "watchers"

CREATE TABLE public.watchers
(
    source_watcher_id text NOT NULL,
    last_query_time timestamp without time zone
);

-- Database Table: "watcher_data"

CREATE TABLE public.watcher_data
(
    entry_id uuid NOT NULL,
    source_watcher_id text,
    source_watcher_item_id text,
    source_item_data text,
    updated timestamp without time zone,
    digest text,
    CONSTRAINT watcher_data_pkey PRIMARY KEY (entry_id)
);
