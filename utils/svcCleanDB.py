import logging
import logging.config
import optparse
import os
import sys
import json

from watchers.core.ServiceDataManager import ServiceDataManager

class ServiceCleanDB(object):

    # ServiceExample Constructor
    def __init__( self, debug = False, options = None  ):

        # Setup internal support variables...
        self._debug = debug  # Debug messages turned on/off.
        self._config_data_file = "config.json"  # Set the name of the config data file.
        self._configData = None  # Init the config data object.

        # Setup the file paths...
        self._defaultPath = "."
        self._systemSettingPath = "."

        dirName = os.path.dirname(sys.argv[0])
        topLevelName = os.path.split(dirName)[1]
        self._systemSettingPath = os.path.join("/etc/masas/watchers", topLevelName)

        # Setup the service logging...
        self._setup_logging(options)

        # Config system file path...
        config_user_file_path = os.path.join(self._systemSettingPath, self._config_data_file)

        # Config default file path...
        config_default_file_path = os.path.join(self._defaultPath, self._config_data_file)

        # Open/Read the config file...

        # If a cmd line config param exists, use that only...
        if (options and options.configFileLocation):
            if (os.path.isfile(options.configFileLocation)):
                with open(options.configFileLocation) as configFile:
                    self._configData = json.load(configFile)
                    # end with
            pass
        else:
            # Otherwise use the defaults..
            # Try the system file path...
            if (os.path.isfile(config_user_file_path)):
                with open(config_user_file_path) as configFile:
                    self._configData = json.load(configFile)
                    # end with
            # end if

            if ((self._configData is None) and (os.path.isfile(config_default_file_path))):
                # Try the default file path...
                with open(config_default_file_path) as configFile:
                    self._configData = json.load(configFile)
                    # end with
                    # end if
        # end if

        if self._configData is None:
            raise RuntimeError("No configuration data.")
        # end if

        # Setup internal support variables based on the configuration file...
        self._watch_name = self._configData["watch_name"]  # User defined Watch name
        if self._watch_name is None:
            self._watch_name = __name__  # Default to the module name

        self._app_dir = self._configData["app_dir"]  # Current application directory

        # Load/Initialize the previous run's data...
        self._serviceDataMgr = ServiceDataManager(self, self._watch_name,
                                                  host=self._configData["host"], port=self._configData["port"],
                                                  database=self._configData["database"],
                                                  user=self._configData["user"],
                                                  password=self._configData["password"])

    # end __init__()

    def close(self):
        self._serviceDataMgr.close()
    # end close

    def _setup_logging(self, options):
        logConfigFileName = "logging.conf"
        logConfigSystemFilePath = os.path.join(self._systemSettingPath, logConfigFileName)
        logConfigDefaultFilePath = os.path.join(self._defaultPath, logConfigFileName)

        validLogConfigPath = logConfigDefaultFilePath

        # Try the system file path...
        if (os.path.isfile(logConfigSystemFilePath)):
            validLogConfigPath = logConfigSystemFilePath
        # end if

        # If a cmd line config param exists, use that only...
        if (options and options.configFileLocation):
            path, file = os.path.split(options.configFileLocation)
            logConfigUserFilePath = os.path.join(path, logConfigFileName)

            if (os.path.isfile(logConfigUserFilePath)):
                validLogConfigPath = logConfigUserFilePath
                # end if
        # end if

        logging.config.fileConfig(validLogConfigPath)
        self.logger = logging.getLogger('watcherService')

    # end _setup_logging(self):

    def run(self):

        # Remove all Items older then the configured days...
        self._serviceDataMgr.removeItemsOlderThan(self._configData["purge_data_store_days"])

    # end run()


# end ServiceCleanDB


if __name__ == '__main__':

    usage = "usage: svcCleanDB [-d] [-m] [-c]"

    parser = optparse.OptionParser(usage)
    parser.add_option("-d", action="store_true", dest="debug",
                      help="print debugging messages")
    parser.add_option("-c", action="store", type="string", dest="configFileLocation",
                      help="Config file location.")
    (options, args) = parser.parse_args()

    service = None
    try:
        service = ServiceCleanDB( debug = options.debug, options = options )
    except Exception, ex:
        service.close()
        logging.error( "An unhandled error occurred during the creation of the Service: " + ex.message )
        sys.exit()
    # end try

    # TODO - MOVE THIS INTO THE BASE WATCHER CLASS...
    if( options.debug ):
        service.run()
    else:
        try:
            service.run()
        except Exception, ex:
            service.close()
            logging.error( "An unhandled error occurred in the Service: " + ex.message )
            sys.exit()
        # end try
    # end if

    if( service ):
        service.close()
    # end if

# end if